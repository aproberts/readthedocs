=================================
Exeter CFD Problems API Reference
=================================

Exeter_CFD_Problems Module
==========================

.. automodule:: Exeter_CFD_Problems

PitzDaily Class
---------------

.. module:: Exeter_CFD_Problems

.. autoclass:: PitzDaily
    :members:
    
KaplanDuct Class
----------------

.. autoclass:: KaplanDuct
    :members:
    
HeatExchanger Class
-------------------

.. autoclass:: HeatExchanger
    :members:
